include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/DAST.latest.gitlab-ci.yml
  - template: Security/Coverage-Fuzzing.gitlab-ci.yml

stages:
  - build
  - inspect
  - test
  - scan
  - publish
  - buildImage
  - scanImage
  - publishImage
  - deploy dev
  - dast
  - deploy test
  - verify
  - deploy prod

variables:
  MAVEN_CLI_OPTS: "-B -s ci_settings.xml"

cache:
  paths:
    - .m2/repository/
    - target/

mavenBuildAndUnitTest:
  stage: build
  image: maven:latest
  script:
    - mvn $MAVEN_CLI_OPTS clean package -DskipTests -Dmaven.repo.local=./.m2/repository
    - mvn $MAVEN_CLI_OPTS verify -DexcludedGroups="Smoke | Staging | BrowserStack | LamdaTest"
  artifacts:
    paths:
      - target/site/jacoco/jacoco.xml
      - .m2/
      - target/
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
  only:
  - merge_requests
  - master
  - develop

sonar:
  stage: inspect
  image: maven:3.6.3-jdk-11
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn $MAVEN_CLI_OPTS verify -DexcludedGroups="Smoke | Staging | LamdaTest | BrowserStack" org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.issuesReport.html.enable=true -Dsonar.projectKey=gitlab-samples_cicd-gitlab-example


publishToGitlab:
  stage: publish
  image: maven:latest
  script:
    - mvn $MAVEN_CLI_OPTS deploy -DskipTests

spotbugs-sast:
  stage: test
  dependencies:
    - mavenBuildAndUnitTest
  variables:
    MAVEN_REPO_PATH: ./.m2/repository
    COMPILE: "false"
  artifacts:
    reports:
      sast: gl-sast-report.json

snykScan:
  stage: scan
  image: node:latest
  allow_failure: true
  script:
    - npm install -g snyk
    - snyk auth 3b721e0f-56bc-4f22-ae2f-4bf52cf27f82
    - snyk test --json

snykIaCScan:
  stage: scan
  image: node:latest
  allow_failure: true
  script:
    - npm install -g snyk
    - snyk auth 3b721e0f-56bc-4f22-ae2f-4bf52cf27f82
    - snyk iac test ./terraform/main.tf --json
  
buildImage:
  stage: buildImage
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

snykImageScan:
  stage: scanImage
  image: node:latest
  allow_failure: true
  services:
    - docker:19.03.12-dind
  script:
    - echo "Performing Snyk Image Scan"
    - npm install -g snyk
    - snyk auth 3b721e0f-56bc-4f22-ae2f-4bf52cf27f82
    - snyk container test $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    
publishImageToGitlab:
  stage: publishImage
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  variables:
    CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE

devDeploy:
  stage: deploy dev
  environment:
    name: development
    url: https://www.google.com
  script:
    - echo "Deploying to dev"
  only:
    - master

testDeploy:
  stage: deploy test
  environment:
    name: testing
    url: https://www.google.com
  script:
    - echo "Deploying to test"
  only:
    - master

verify:
  stage: verify
  script:
    - echo "Automatic verification"
prodDeploy:
  stage: deploy prod
  environment:
    name: production
    url: https://www.google.com
  script:
    - echo "Deploying to Prod"
  only:
    - master
